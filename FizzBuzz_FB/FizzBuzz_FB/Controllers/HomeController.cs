﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Text;
using FizzBuzz_FB.Models;
using FB.Services.Interface;
using FB.Services;
using FB.Services.Models;

namespace FizzBuzz_FB.Controllers
{
    public class HomeController : Controller
    {
        public ActionResult Index()
        {
            //um.Numbers = Convert.ToInt32(fc["Numbers"]);
            //INumberServices numService = new NumberServices();
            //um.num=numService.GetNumberList(um.Numbers);
            ////ViewBag.NumList = um.num;




            //IDivisibleby3Services numDivisibleby3Service = new DivisibleBy3Services();
            //ViewBag.NumList2 = numDivisibleby3Service.GeDivisibleby3List(um.Numbers);
            var modelData = new FizzBuzz() { Numbers=0 };

            return View(modelData);
        }
        [HttpPost]
        public ActionResult Index(FormCollection fc, FizzBuzz um)
        {
            um.Numbers = Convert.ToInt32(fc["Numbers"]);
            INumberServices numService = new NumberServices();
            //ViewBag.NumList = numService.GetNumberList(um.Numbers);
            var numList = numService.GetNumberList(um.Numbers);

            IDivisibleby3Services numDivisibleby3Service = new DivisibleBy3Services();
            var numDivBy3 = numDivisibleby3Service.GeDivisibleby3List(numList);

            IDivisibleby5Services numDivisibleby5Service = new DivisibleBy5Services();
            ViewBag.NumList = numDivisibleby5Service.GeDivisibleby5List(numDivBy3);

            
            //ViewBag.NumList2 = numDivisibleby3Service.GeDivisibleby3List(um.Numbers);
            //ViewBag.NumList = um.num;




            //IDivisibleby3Services numDivisibleby3Service = new DivisibleBy3Services();
            //ViewBag.NumList2 = numDivisibleby3Service.GeDivisibleby3List(um.Numbers);


            return View(um);
        }

        //[HttpPost]
        //public ActionResult Index(FormCollection fc, UserModel um)
        //{
        //    if (ModelState.IsValid)
        //    {
        //        um.Numbers = Convert.ToInt32(fc["Numbers"]);
        //        ViewBag.Num = um.Numbers;


        //        StringBuilder sb = new StringBuilder();

        //        for (int j = 1; j <= um.Numbers; j++)
        //        {
        //            string str = "";
        //            if (j % 3 == 0)
        //            {
        //                str += "Fizz";
        //            }
        //            if (j % 5 == 0)
        //            {
        //                str += "Buzz";
        //            }
        //            if (str.Length == 0)
        //            {
        //                str = j.ToString();
        //            }

        //            sb.Append((str) + " ");

        //        }

        //        ViewBag.ResultSet = sb.ToString();

        //        return View(um);
        //    }
        //    else
        //    {
        //        return View();
        //    }
        //}
    }
}