﻿using FB.Services.Interface;
using FB.Services.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FB.Services
{
    public class DivisibleBy5Services : IDivisibleby5Services
    {
        public List<FizzBuzz> GeDivisibleby5List(List<FizzBuzz> displayNumbyFive)
        {
            
                for (int j = 1; j <= displayNumbyFive.Count(); j++)
                {
                    if (j % 5 == 0)
                    {
                       
                    displayNumbyFive.Find(i => i.Numbers.Equals(j)).fizzbuzz = "Buzz";
                    }

                }
                return displayNumbyFive;
        }
    }
}
