﻿using FB.Services.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FB.Services.Interface
{
   public interface INumberServices
    {

        List<FizzBuzz> GetNumberList (int num);
         
    }
}
