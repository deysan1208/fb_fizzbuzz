﻿using FB.Services.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FB.Services.Interface
{
    public interface IDivisibleby3Services
    {
        List<FizzBuzz> GeDivisibleby3List(List<FizzBuzz> numdivthree);
    }
}
